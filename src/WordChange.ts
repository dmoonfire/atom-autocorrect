/**
 * Represents a single word inside the buffer that has been changed. This
 * includes also the location information for replacement.
 */
export class WordChange {
    public word: string;
    public originalWord: string;
    public changed: boolean = false;
    public range: TextBuffer.IRange;

    constructor(word: string, range: TextBuffer.IRange) {
        this.word = word;
        this.originalWord = word;
        this.range = range;
    }

    public replace(newWord: string): void {
        this.word = newWord;
        this.changed = true;
    }
}
