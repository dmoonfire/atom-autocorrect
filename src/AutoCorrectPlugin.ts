const Atom = require("atom");
import { AutoCorrectState } from "./AutoCorrectState";
import { AutoCorrectView } from "./AutoCorrectView";
import { WordCorrection } from "./WordCorrection";
import { IEditor } from "./fake-atom";

export class AutoCorrectPlugin implements AutoCorrectState {
    public wordCorrections: WordCorrection[] = [];
    private editorViews = new WeakMap<AtomCore.IEditor, AutoCorrectView>();
    private views: AutoCorrectView[] = [];
    private subscriptions = new Atom.CompositeDisposable();

    public activate(state: any) {
        // Events subscribe in atom's system can easily be cleaned up with a
        // CompositeDisposable on the deactivate.
        this.subscriptions = new Atom.CompositeDisposable();

        // Register the commands for this plugin. These are described in the
        // keybindings.
        this.subscriptions.add(
            atom.commands.add(
                'atom-workspace',
                {
                    'autocorrect:toggle': () => this.toggle()
                }));

        // We also need to register listeners for new buffers so we can activate
        // when the user opens a new window.
        this.subscriptions.add(
            atom.workspace.observeTextEditors(
                (editor: IEditor) => { this.attachEditor(editor); }));
    }

    public deactivate() {
        for (let view of this.views) {
            view.destroy();
        }

        this.subscriptions.dispose();
        this.editorViews = new WeakMap<AtomCore.IEditor, AutoCorrectView>();
        this.views = [];
    }

    public getEditorView(editor: IEditor): AutoCorrectView {
        return this.editorViews.get(editor) as AutoCorrectView;
    }

    public registerWordCorrection(wordCorrection: WordCorrection): void {
        this.wordCorrections.push(wordCorrection);
    }

    private attachEditor(editor: IEditor) {
        const view = new AutoCorrectView(this, editor);
        this.editorViews.set(editor, view);
        this.views.push(view);
    }

    private toggle() {
        console.log('toggled via class');
    }
}
