import { WordChange } from "./WordChange";

/**
 * Describes the interface of an external plugin that provides autocorrection
 * for words.
 */
export interface WordCorrection {
    /**
     * Contains the unique identifier for this plugin.
     */
    id: string;

    /**
     * Determines if the word needs to be corrected by the plugin. If it has,
     * then it is assumed that change.replace() is called.
     */
    correctWord(change: WordChange): void;
}
